/*
 * CONFIDENTIAL
 * Copyright 2019 Webalo, Inc.  All rights reserved.
 */

package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.example.demo.controller.DbSequence;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;

/**
 *
 */
@Service
public class SequenceGeneratorService {


    @Autowired
    private MongoOperations mongoOperations;

    public int getSequenceNumber(String sequenceName) {
        //get sequence no
        List<Criteria> criteria = new ArrayList<>();
        criteria.add(Criteria.where("id").is(sequenceName));
        return this.buildQueryAndUpdateTheSequence(criteria);
    }

    public int getSequenceNumber(int hotelId, String sequenceName) {
        //get sequence no
        List<Criteria> criteria = new ArrayList<>();
        String id = String.valueOf(hotelId) + sequenceName;
        criteria.add(Criteria.where("id").is(id));
        return this.buildQueryAndUpdateTheSequence(criteria);
    }

    private int buildQueryAndUpdateTheSequence(List<Criteria> criteria) {
        Query query = new Query
            (new Criteria().andOperator
             (criteria.toArray(new Criteria[criteria.size()])));

        //update the sequence no
        Update update = new Update().inc("seq", 1);
        //modify in document
        DbSequence counter = mongoOperations
                .findAndModify(query,
                        update, options().returnNew(true).upsert(true),
                        DbSequence.class);

        return !Objects.isNull(counter) ? counter.getSeq() : 1;
    }
}