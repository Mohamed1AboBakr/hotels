/*
 * CONFIDENTIAL
 * Copyright 2019 Webalo, Inc.  All rights reserved.
 */

package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Hotel;
import com.example.demo.entity.Room;
import com.example.demo.repository.HotelRepository;
import com.example.demo.service.SequenceGeneratorService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 */
@RestController
@RequestMapping(path="api/hotels")
public class HotelController {

    @Autowired
    private HotelRepository hotelRepo;
    @Autowired
    private SequenceGeneratorService service;

    @GetMapping(value="/all")
    public List<Hotel> getAllHotels() {
        return this.hotelRepo.findAll();
    }

    @GetMapping(value="/hotel")
    public Optional<Hotel> getHotel(int id) {
        return this.hotelRepo.findById(id);
    }

    @GetMapping(value="/rooms")
    public List<Room> getRooms(@RequestParam Integer hotelId) {
        return this.hotelRepo.findById(hotelId).get().getRooms();
    }

    @PostMapping(value="/add")
    public String addHotel(@RequestBody Hotel hotel) {
        hotel.setId(service.getSequenceNumber(Hotel.SEQUENCE_NAME));
        if (hotel.getRooms() == null) {
            hotel.setRooms(new ArrayList<Room>());
        }
        Hotel newHotel = this.hotelRepo.insert(hotel);
        return "new hotel with id: " + newHotel.getId() + "is added.";
    }

    @PostMapping(value="/add-room")
    @ResponseBody
    public String addRoom(@RequestBody Room room,
                          @RequestParam Integer hotelId) {

        Optional<Hotel> hotel = hotelRepo.findById(hotelId);
        room.setId(service.getSequenceNumber
                   (hotel.get().getId(), Room.SEQUENCE_NAME));

        hotel.get().addRoom(room);
        hotelRepo.save(hotel.get());
        return "new room with id: " + room.getId() + "is added"
            + " to hotel with id: " + hotelId;
    }
}
