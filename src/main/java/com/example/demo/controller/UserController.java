/*
 * CONFIDENTIAL
 * Copyright 2019 Webalo, Inc.  All rights reserved.
 */

package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.SequenceGeneratorService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 */
@RestController
@RequestMapping(path="api/users")
public class UserController {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private SequenceGeneratorService service;

    @GetMapping(value="/all")
    public List<User> getAllUser() {
        return this.userRepo.findAll();
    }

    @GetMapping(value="/user")
    @ResponseBody
    public Optional<User> getUser(@RequestParam int id) {
        return this.userRepo.findById(id);
    }

    @PostMapping(value="/add")
    public String addUser(@RequestBody User user) {
        user.setId(service.getSequenceNumber(User.SEQUENCE_NAME));
        User newUser = this.userRepo.insert(user);
        return "new user with id: " + newUser.getId() + "is added.";
    }
}
