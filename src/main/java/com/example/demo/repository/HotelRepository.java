/*
 * CONFIDENTIAL
 * Copyright 2019 Webalo, Inc.  All rights reserved.
 */

package com.example.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Hotel;

/**
 *
 */
@Repository
public interface HotelRepository extends MongoRepository<Hotel, Integer> {
}
